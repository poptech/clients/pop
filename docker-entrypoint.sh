#!/bin/sh

set -e

target=/var/www/html

# check if directory exists
if [ -d "$target" ]; then
    cd /tmp
    if [ -d "$target/wp-admin"]; then
    	echo "wordpress already installed"
    else
    	cp -vrf /wordpress/. /var/www/html
    fi
    echo "Copy current change"
    cp -rf /tmp/wordpress/. /var/www/html
else
    # directory doesn't exist, we will have to do something here
    echo need to creates the directory...
fi

if [ -e "$target/wp-config.php" ]; then
	echo "wp-config is already generated"
else
	echo "Generate wp-config"
	echo "$WP_DBNAME $WP_DBUSER $WP_DBPASS $WP_DBHOST"
	cd $target && wp --allow-root config create --dbname=$WP_DBNAME --dbuser=$WP_DBUSER --dbpass=$WP_DBPASS --dbhost=$WP_DBHOST
fi

chown -R www-data:www-data /var/www/html

exec "$@"
