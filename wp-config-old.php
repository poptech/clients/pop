<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pop');

/** MySQL database username */
define('DB_USER', 'pop@pop-pop');

/** MySQL database password */
define('DB_PASSWORD', 'WR!bjMEg23A2');

/** MySQL hostname */
define('DB_HOST', 'pop-pop.mysql.database.azure.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '19793166075CB765F17D4B392DF092B878E5526C4FFEA870C83F42ED16CD0E2B');
define('SECURE_AUTH_KEY',  'A1EFFE5D1444D27B62DFC30B4AB4B1AC7D7AB35137746C6CB839645D270DE6C4');
define('LOGGED_IN_KEY',    'F299E246791161607CA43B423CD52E8DED5279041956626691A5D5A0698125B9');
define('NONCE_KEY',        'D9E0FB533A0D271F312CA183EC30D8FC3B2080CD2E825C44898AAAEFC6251C8B');
define('AUTH_SALT',        '53AF5B14494217CF02413A2E553A21AB33E893B4641996978CFEDD22C42CF1A9');
define('SECURE_AUTH_SALT', '2A760932FCC3CB1BA68B239C2F999CFDBF042746A65C3E0719B94DC702868060');
define('LOGGED_IN_SALT',   '738F86D85C731C741CD8CF4F245844C21F49A2C8AB076B5D9E9793E7E728DA28');
define('NONCE_SALT',       '1BEABE283B878A928F7ED0369021B699FA0E6A4EFFB73314DA9F737765103308');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
