FROM registry.gitlab.com/poptech/dev-ops/wordpress:4.8.3

RUN mkdir /tmp/wordpress

ADD . /tmp/wordpress

ADD docker-entrypoint.sh /tmp/docker-entrypoint.sh

RUN chmod +x /tmp/docker-entrypoint.sh

VOLUME ["/var/www/html"]

ENTRYPOINT ["/tmp/docker-entrypoint.sh"]

CMD ["apache2-foreground"]
