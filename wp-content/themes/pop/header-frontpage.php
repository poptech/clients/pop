<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-precomposed.png">
		
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>

		<div id="particles-js"></div> 
		<div class="row">
		
  <div class="row"><div class="small-12 small-centered">
<div class="home-menu">
			<ul class="inline-list center">
				<li><a href="<?php echo site_url();?>/vision/"><div class="menu animate" style="transform: translateY(0px);"><span>VISION</span></div></a></li>
				<li><a href="<?php echo site_url();?>/activite/"><div class="menu animate" style="transform: translateY(0px);"><span>ACTIVITÉ</span></div></a></li>
				<li><a href="<?php echo site_url();?>/roadbook/"><div class="menu animate" style="transform: translateY(0px);"><span>ROADBOOK</span></div></a></li>
                <li><a href="<?php echo site_url();?>/crew/"><div class="menu animate" style="transform: translateY(0px);"><span>ÉQUIPE</span></div></a></li>
				<li><a href="<?php echo site_url();?>/eco-systeme/"><div class="menu animate" style="transform: translateY(0px);"><span>ÉCO-SYSTÈME</span></div></a></li>
			
			</ul>
		</div>
</div>
	</div>
		<section class="container" role="document">
