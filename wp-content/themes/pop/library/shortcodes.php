<?php
function section_shortcode( $atts, $content = null ) {
	$a = shortcode_atts( array(
		'title' => '',
	), $atts );
	//$output = '<div class="section-separator"><div class="spot"><span>' . $content . '</span></div></div>';

	$output = '<div class="row">';
	$output .= '<div class="small-12 medium-3 columns">';
	$output .= '<div class="l-title section-title">'.esc_attr($a['title']).'</div>';
	$output .= '</div>';
	$output .= '<div class="small-12 medium-6 columns end">';
	$output .= '	<div class="line"></div>';
	$output .= '	<p>' .$content. '</p>';
	$output .= '</div>';
	$output .= '</div>';

	return $output;
}
add_shortcode( 'section', 'section_shortcode' );

function section_header_shortcode( $atts, $content = null ) {
	
	$a = shortcode_atts( array(
		'title' => '',
	), $atts );

	$output = '<div class="row"><div class="small-12 medium-6 medium-offset-3 columns">';
	$output .=  '<div class="section-separator">';
	$output .= '<div class="spot"><span>' . esc_attr($a['title']) . '</span></div>';
	$output .= '</div>';
	$output .= '<p>'.$content.'</p>';
	$output .= '</div></div>';
	
	return $output;
}
add_shortcode( 'section_header', 'section_header_shortcode' );

?>
