<?php
/**
 * The home template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */
?>

<?php get_template_part( 'header', 'frontpage' ); ?>

<div class="row home-main">
	<div class="small-6 small-offset-3 end medium-4 medium-offset-4 columns home-main-column" role="main">
		<div class="home-main-content">
			<div class="logo ">POP</div>
			<?php if ( have_posts() ) : ?>
						
				<?php while ( have_posts() ) : the_post(); ?>
					<p class="entry-content"><?php the_content(); ?></p>
				<?php endwhile; ?>

				
			<?php endif;?>
		</div>
		
	</div>

</div>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/particles.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/velocity.min.js"></script>
<script>
	particlesJS.load('particles-js', '<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/particlesjs-config.json', function() {
	  console.log('callback - particles.js config loaded');
	});
	
	

</script>
<?php get_footer(); ?>