<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0
 */

?>

</section>
</div>

<div class="fixed-footer" id="footer">
	<footer class="row center">
		<div class="columns large-12 large-centered medium-12 medium-centered small-12">
			<ul class="inline-list center">
			 <li><a href="<?php echo home_url('popplace' );?>"><div class="spot popplace animate"><span>place</span></div></a></li>
			 <li><a href="<?php echo home_url('popup' );?>"><div class="spot popup animate"><span>up</span></div></a></li>
			 <li><a href="<?php echo home_url('poptech' );?>"><div class="spot poptech animate"><span>Tech</span></div></a></li>
			 <li><a href="<?php echo home_url('popschool' );?>"><div class="spot popschool animate"><span>school</span></div></a></li>
			 <li><a href="<?php echo home_url('poplab' );?>"><div class="spot poplab animate"><span>lab</span></div></a></li>
			</ul>
		</div>
	<div class="bottom">
        <div class="small-12 medium-3 columns">
			<div class="inline-list center small-screen">
				<a  href="<?php echo site_url();?>/mentions-legales" class="animate" style="transform: translateY(0px);"><span>Mentions Légales</span></a>
				
            </div>
		</div>
		<div class="small-12 small-centered medium-6 medium-uncentered columns">
			<ul class=" bottom-menu inline-list center">
				<li><a href="<?php echo site_url();?>/job"><div class="menu animate" style="transform: translateY(0px);"><span>JOBS</span></div></a></li>
				<li><a href="<?php echo site_url();?>/press"><div class="menu animate" style="transform: translateY(0px);"><span>PRESSE</span></div></a></li>
				<li><a href="<?php echo site_url();?>/contact"><div class="menu animate" style="transform: translateY(0px);"><span>CONTACT</span></div></a></li>
				<li><a href="<?php echo site_url();?>/suivez-nous"><div class="menu animate" style="transform: translateY(0px);"><span>SUIVEZ-NOUS</span></div></a></li>
			
			</ul>
		</div>
        <div class="small-12 small-centered medium-3 medium-uncentered large 2 columns  ">
            <div class="inline-list center med-screen">
				<a  href="<?php echo site_url();?>/mentions-legales" class="animate" style="transform: translateY(0px);"><span>Mentions Légales</span></a>
				
            </div>
			<ul class=" social inline-list center">
				<li><a href="https://twitter.com/Plus2POP?lang=fr"><div class="social animate" style="transform: translateY(0px);"><span><i class="fa fa-4 fa-twitter-square"></i></span></div></a></li>
			</ul>
		</div>
    </div>
		
	</footer>
</div>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/vendor/velocity.min.js"></script>
<script>
$(document).ready(function(){
		$('.spot.animate').on('mouseenter',function(event) {
			$(this).velocity({ 
    			translateY: "-5px"
			});
		}); 
		$('.spot.animate').on('mouseleave',function(event) {
			$(this).velocity({ 
    			translateY: "0"
			});
		}); 		
	});
</script>
<?php wp_footer(); ?>

</body>
</html>
