<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<?php 
	$team_term = array_shift(get_the_terms( $post->ID, 'team' ));
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header>
		<a href="<?php the_permalink(); ?>">	
			<h2><?php the_title(); ?></h2>
			<h4><?php echo get_field('roadmap_subtitle'); ?></h4>
		</a>
	</header>
	<div class="entry-status">
		<?php 
			$field = get_field_object('roadmap_status');
			$value = get_field('roadmap_status');
			$label = $field['choices'][$value];			
			switch ($value) {
				case 'started':
					$class="one_third";
					break;
				case 'pending':
					$class="two_third";
					break;
				case 'stopped':
					$class="full";
					break;
				case 'closed':
					$class="full";
					break;
				default:
					$class="full";
					break;
			}
		?>
		<div class="roadmap-status ">
			<div class="<?php echo $class; ?>"><?php echo $label; ?></div>
		</div>

	</div>
	<div class="filter-details">
		<div class="team <?php echo $team_term->slug; ?>"><?php echo $team_term->name; ?></div>

	</div>
	<hr />
</article>