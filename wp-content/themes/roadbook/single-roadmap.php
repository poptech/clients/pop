<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="row">
	<div class="small-12 large-8 columns" role="main">

	<?php do_action( 'foundationpress_before_content' ); ?>

	<?php while ( have_posts() ) : the_post(); ?>

		<?php 
			$adventure_term = array_shift(get_terms( 'aventure'));
			$team_term = array_shift(get_terms( 'team'));
		?>

		<article <?php post_class() ?> id="post-<?php the_ID(); ?>">
			<header>
				<div class="filter-details">
					<div class="adventure <?php echo $team_term->slug; ?>"><?php echo $adventure_term->name; ?></div>
					<div class="team <?php echo $team_term->slug; ?>"><?php echo $team_term->name; ?></div>
				</div>
				<h1 class="roadmap-title"><?php the_title(); ?></h1>
				<h4><?php echo get_field('roadmap_subtitle'); ?></h4>
			</header>
			<?php do_action( 'foundationpress_post_before_entry_content' ); ?>
			<div class="roadmap-content">

			<?php if ( has_post_thumbnail() ) : ?>
				<div class="row">
					<div class="column">
						<?php the_post_thumbnail( '', array('class' => 'th') ); ?>
					</div>
				</div>
			<?php endif; ?>
			<?php the_content( ); ?>

			</div>
			<!-- <div class="roadmap-steps">
			
				<h3>Les temps forts</h3>
				<?php

				if( have_rows('roadmap_steps') ):
				    while ( have_rows('roadmap_steps') ) : the_row(); ?>
						<div class="row">
							<div class="small-12 medium-3 columns roadmap-step-date">
								<?php echo the_sub_field('roadmap_step_date'); ?>
							</div>
							<div class="small-1 columns show-for-medium-up">
								<span class="dot passed">&nbsp;</span>
							</div>
							<div class="small-12 medium-8 columns">
								<h4><?php echo the_sub_field('roadmap_step_title'); ?></h4>
								<p><?php echo the_sub_field('roadmap_step_description'); ?>	</p>
							</div>
				        </div>
				<?php
				    endwhile;
				endif;

				?>
			</div> -->
			<footer>
				<?php wp_link_pages( array('before' => '<nav id="page-nav"><p>' . __( 'Pages:', 'foundationpress' ), 'after' => '</p></nav>' ) ); ?>
				<p><?php the_tags(); ?></p>
			</footer>
			<?php do_action( 'foundationpress_post_before_comments' ); ?>
			<?php comments_template(); ?>
			<?php do_action( 'foundationpress_post_after_comments' ); ?>
		</article>
	

		<?php do_action( 'foundationpress_after_content' ); ?>

		</div>
		<div class="small-12 large-4 columns">
				<?php 
					$field = get_field_object('roadmap_status');
					$value = get_field('roadmap_status');
					$label = $field['choices'][$value];	

					switch ($value) {
						case 'started':
							$class="one_third";
							break;
						case 'pending':
							$class="two_third";
							break;
						case 'stopped':
							$class="full";
							break;
						case 'closed':
							$class="full";
							break;
						default:
							$class="full";
							break;
					}
				?>
				<div class="roadmap-status ">
					<div class="<?php echo $class; ?>"><?php echo $label; ?></div>
				</div>
			
			<div class=".roadmap-download">
				<h3>TÉLÉCHARGER LA FICHE PROJET</h3>
				<?php $file = get_field('fiche_projet');
					if ( $file ):
				?>
					<a id="project_file" href="<?php echo $file; ?>">Fiche projet</a>
				<?php endif; ?>
			</div>

			<div class="roadmap-topo">
				<h3>TOPO</h3>
				<?php the_excerpt(); ?>	
			</div>
			
			<div class="roadmap-crew">
				<h3>A bord</h3>

				
				<?php 
				$members = get_field('roadmap_crew_captains');

				if( $members ): ?>
					<h4>Les capitaines</h4>
				    <ul>
				    <?php foreach( $members as $member): ?>
				        <li>
				            <a href="<?php echo get_permalink($member); ?>" alt="<?php get_the_title($member); ?>">
				            	<span> <?php the_field('members_initial', $member); ?></span>
				            </a>
				        </li>
				    <?php endforeach; ?>
				    </ul>
				<?php endif;

				?>

				
				<?php 
				$members = get_field('roadmap_crew');

				if( $members ): ?>
					<h4>L'équipage</h4>
				    <ul>
				    <?php foreach( $members as $member): ?>
				        <li>
				            <a href="<?php echo get_permalink($member); ?>" alt="<?php get_the_title($member); ?>">
				            	<span> <?php the_field('members_initial', $member); ?></span>
				            </a>
				        </li>
				    <?php endforeach; ?>
				    </ul>
				<?php endif;?>
					<!-- <p>Le grand large, l'aventure, ça vous tente ?</p>
					<ul>
						<li><a href="#">
							+ 						</a></li>
					</ul>-->

				

				<!-- 
				<h4>La vigie</h4>
				<?php 
				$members = get_field('roadmap_crew_watchers');

				if( $members ): ?>
				    <ul>
				    <?php foreach( $members as $member): ?>
				        <li>
				            <a href="<?php echo get_permalink($member); ?>" alt="<?php get_the_title($member); ?>">
				            	<span> <?php the_field('members_initial', $member); ?></span>
				            </a>
				        </li>
				    <?php endforeach; ?>
				    </ul>

				<?php endif;?>
					<p>Garder le cap, ça vous tente ?</p>
					<ul>
						<li><a href="#">
							+ 
						</a></li>
					</ul>
				-->
				
			</div>

		</div>
	<?php endwhile;?>
</div>
<?php get_footer(); ?>