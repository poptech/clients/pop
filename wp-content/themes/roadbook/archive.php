<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php
	$teams = get_terms( 'team' );
?>

<?php 
	// $a = urldecode($wp_query->query_vars['a']);
	$t = urldecode($wp_query->query_vars['t']);
	echo $t;
?>

<div class="row filters">
	<div class="small-4 columns">
		<p>Le roadbook de POP rassemble nos projets, ceux que nous menons pour nos clients, que nous co-produisons avec nos partenaires, et ceux que nous portons au sein de POP, et ceux auxquels nous vous proposons de contribuer.</p>
	</div>
	<!-- <div class="small-4  columns">
		<h4>PAR AVENTURE</h4>
		<ul class="small-block-grid-2">
			<?php foreach ($adventures as $adventure) : ?>
				<li >
					<?php if($a==$adventure->slug) {$class = 'active';} else {$class='';} ?>
					<a href="/roadbook/a/<?php echo $adventure->slug; ?>" class="<?php echo $class; ?>">
						<span class="icon-<?php echo $adventure->slug; ?>"></span>
						<?php echo $adventure->name; ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div> -->
	<div class="small-4 columns">
		<h4>PAR ACTIVITÉ</h4>
		<ul class="small-block-grid-2">
			<?php foreach ($teams as $team) : ?>
				<li >
					<?php if($t==$team->slug) {$class = 'active';} else {$class='';} ?>
					<a href="/roadbook/?team=<?php echo $team->slug; ?>" class="<?php echo $class; ?>">
						<span class="team-dot <?php echo $team->slug; ?>"></span>
						<?php echo $team->name; ?>
					</a>
				</li>
			<?php endforeach; ?>
		</ul>
	</div>
</div>


<div class="row">
	<div class="small-12 large-12 columns" role="main">
	<?php if ( have_posts() ) : ?>

		<?php do_action( 'foundationpress_before_content' ); ?>
		<ul class="small-block-grid-1 medium-block-grid-3 roadmaps">
			<?php while ( have_posts() ) : the_post(); ?>
				<li>
					<?php get_template_part( 'content-roadmap', get_post_format() ); ?>	
				</li>
			<?php endwhile; ?>
		</ul>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		
		<?php do_action( 'foundationpress_before_pagination' ); ?>

	<?php endif;?>



	<?php if ( function_exists( 'foundationpress_pagination' ) ) { foundationpress_pagination(); } else if ( is_paged() ) { ?>
		<nav id="post-nav">
			<div class="post-previous"><?php next_posts_link( __( '&larr; Older posts', 'foundationpress' ) ); ?></div>
			<div class="post-next"><?php previous_posts_link( __( 'Newer posts &rarr;', 'foundationpress' ) ); ?></div>
		</nav>
	<?php } ?>

	<?php do_action( 'foundationpress_after_content' ); ?>

	</div>
</div>
<?php get_footer(); ?>
