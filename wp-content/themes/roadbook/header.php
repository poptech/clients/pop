<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />

		<link rel="icon" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/favicon.ico" type="image/x-icon">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-144x144-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-114x114-precomposed.png">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-72x72-precomposed.png">
		<link rel="apple-touch-icon-precomposed" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/icons/apple-touch-icon-precomposed.png">
		
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<?php do_action( 'foundationpress_after_body' ); ?>
	
	<div class="off-canvas-wrap" data-offcanvas>
	<div class="inner-wrap">
	
	<?php do_action( 'foundationpress_layout_start' ); ?>
	
	<nav class="tab-bar">
		<section class="left-small">
			<a class="left-off-canvas-toggle menu-icon" href="#"><span></span></a>
		</section>
		<section class="middle tab-bar-section">
			
			<h1 class="title">
				<?php bloginfo( 'name' ); ?>
			</h1>

		</section>
	</nav>

	<?php get_template_part( 'parts/off-canvas-menu' ); ?>

	<div class="row theme-header">
		<div class="small-12 medium-2 columns">
			<a href="<?php echo site_url(); ?>"><img src="<?php echo get_stylesheet_directory_uri();?>/assets/img/logo.png" alt="POP - Le Roadbook"></a>
		</div>
		<div class="small-12 medium-10 columns">
			<h1>Le roadbook</h1>
			<h3>Nos références et autres découvertes</h3>
		</div>
	</div>
	<div class="row">
		<div class="small-12 columns" >
			<ul id="menu-principal" class="main-bar">
				<li><a href="<?php echo site_url();?>/vision/"><div class="icon-nav menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item "><span>VISION</span></div></a></li>
				<li><a href="<?php echo site_url();?>/activite/"><div class="icon-nav menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item " ><span>ACTIVITÉS</span></div></a></li>
				<li><a href="<?php echo site_url();?>/roadbook/"><div class="icon-map icon-nav menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item  active"><span>ROADBOOK</span></div></a></li>
                <li><a href="<?php echo site_url();?>/crew/"><div class="icon-anchor icon-nav menu-item menu-item-type-custom menu-item-object-custom "><span>ÉQUIPE</span></div></a></li>
				<li><a href="<?php echo site_url();?>/eco-systeme/"><div class="icon-nav menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item "><span>ÉCO-SYSTÈME</span></div></a></li>
			</ul>
		</div>
	</div>
	

<section class="container" role="document">
	<?php do_action( 'foundationpress_after_header' ); ?>
