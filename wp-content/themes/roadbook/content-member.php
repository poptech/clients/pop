<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="row">
		<div class="small-12 large-2 columns">
			<?php if ( has_post_thumbnail() ) : ?>
				<?php the_post_thumbnail( '', array('class' => 'member-thumbnail') ); ?>
			<?php else : ?>
				<div class="member-thumbnail icon-head"></div>
			<?php endif; ?>

		</div>
		<div class="small-12 large-6 columns">
			<h3><?php the_title( );?></h3>
			<p><?php the_content(); ?></p>
			<!--
			<ul class="member-social">
				<li><a href="mailto:pierre.trendel@gmail.com">Mail</a></li>
				<li><a href="http://www.twitter/pierretrendel/">Twitter</a></li>
			</ul>
			-->
		</div>

		<?php

		$args_captains = array(
						'numberposts'	=> -1,
						'post_type'		=> 'roadmap',
						'meta_query' => array(
							array(
								'key' => 'roadmap_crew_captains',
								'value' => '"' . get_the_id() . '"',
								'compare' => 'LIKE'
							)
						)
					);
		$roadmap_query_captains = new WP_Query($args_captains);


		$args_crew = array(
						'numberposts'	=> -1,
						'post_type'		=> 'roadmap',
						'meta_query' => array(
							array(
								'key' => 'roadmap_crew',
								'value' => '"' . get_the_id() . '"',
								'compare' => 'LIKE'
							)
						)
					);

					
		$roadmap_query_crew = new WP_Query($args_crew);

		$args_watchers = array(
						'numberposts'	=> -1,
						'post_type'		=> 'roadmap',
						'meta_query' => array(
							array(
								'key' => 'roadmap_crew_watchers',
								'value' => '"' . get_the_id() . '"',
								'compare' => 'LIKE'
							)
						)
					);

		$roadmap_query_watchers = new WP_Query($args_watchers);

		?>

		<div class="small-12 large-4 columns">
				<?php 
					if($roadmap_query_captains->have_posts() || $roadmap_query_crew->have_posts() || $roadmap_query_watchers->have_posts()):

				?>
			<div class="member-roadmaps">
				<h3>A bord de</h3>
				<ul>

				
					<?php while ( $roadmap_query_captains->have_posts() ) : $roadmap_query_captains->the_post(); ?>
						<li>
							<span class="icon-cog roadmap-crew-icon"></span>
							<span><a href="<?php the_permalink(); ?>"><?php the_title();?></a></span>
						</li>
					<?php endwhile; ?>				
				

				
					<?php while ( $roadmap_query_crew->have_posts() ) : $roadmap_query_crew->the_post(); ?>
						<li>
							<span class="icon-anchor roadmap-crew-icon"></span>
							<span><a href="<?php the_permalink(); ?>"><?php the_title();?></a></span>
						</li>
					<?php endwhile; ?>				


				
					<?php while ( $roadmap_query_watchers->have_posts() ) : $roadmap_query_watchers->the_post(); ?>
						<li>
							<span class="icon-eye roadmap-crew-icon"></span>
							<span><a href="<?php the_permalink(); ?>"><?php the_title();?></a></span>
						</li>
					<?php endwhile; ?>				
								
				</ul>
			</div>
			<?php wp_reset_query();?>
		<?php endif; ?>
		</div>

	</div>
	<hr />
</article>