<?php

// add_filter('rewrite_rules_array','wp_insertMyRewriteRules');
// add_filter('query_vars','wp_insertMyRewriteQueryVars');
// add_filter('init','flushRules');

// Remember to flush_rules() when adding rules
function flushRules(){
	global $wp_rewrite;
   	$wp_rewrite->flush_rules();
}

// Adding a new rule
function wp_insertMyRewriteRules($rules)
{
	$newrules = array();
	// $newrules['a/(.+)/t/(.+)'] = 'roadbook/index.php?a=$matches[1]&t=$matches[2]';
	// $newrules['a/(.+)'] = 'roadbook/index.php?a=$matches[1]';
	$newrules['roadbook/t/(.+)'] = 'roadbook/index.php?post_type=roadmap&t=$matches[1]';
    $newrules['crew/'] = 'roadbook/index.php?post_type=member';

	
	$finalrules = $newrules + $rules;
        return $finalrules;
}

// Adding the var so that WP recognizes it
function wp_insertMyRewriteQueryVars($vars)
{
    // array_push($vars, 'a');
    array_push($vars, 't');
    return $vars;
}

function roadmap_modify_frontpage_post_type( $query ) {

    // $query->set( 'post_type', get_query_var('post_type') );

	// Check if on frontend and main query is modified
	if( ! is_admin() && $query->is_main_query() && ! is_archive('member')) {
        $query->set( 'posts_per_page', '9' );
        $query->set( 'orderby', 'date' );
        $query->set( 'order', 'DESC' );
        // if(get_query_var('a')) {
        // 	$query->set( 'tax_query', array(array("taxonomy" => "aventure", "field" => "slug", "terms" => get_query_var('a') )));
        // }
        // if(get_query_var('t')) {
        // 	$query->set( 'tax_query', array(array("taxonomy" => "team", "field" => "slug", "terms" => get_query_var('t') )));
        // }

		// if(get_query_var('t') && get_query_var('a')) {
  //       	$query->set( 'tax_query', array('relation' => 'AND',array("taxonomy" => "team", "field" => "slug", "terms" => get_query_var('t') )));
  //           // array("taxonomy" => "aventure", "field" => "slug", "terms" => get_query_var('a') )
  //       }      

    }
    if( ! is_admin() && $query->is_main_query() && is_archive('member')) {
        $query->set( 'posts_per_page', '10' );
        $query->set( 'orderby', 'title' );
        $query->set( 'order', 'ASC' );

    }
    return $query;
}
add_action( 'pre_get_posts', 'roadmap_modify_frontpage_post_type' );

?>